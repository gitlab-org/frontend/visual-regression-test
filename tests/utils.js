const fs = require("fs");
const { SCREENSHOTS_REPORT_TMP_FILE } = require("./constants");

// Maybe this should create one file per test suite to avoid conflicts when
// running tests in parallel.
const persistBrowserCapabilities = (fontName, browser) => {
  const { browserName, browserVersion } = browser.capabilities;
  const { os, osVersion } = browser.requestedCapabilities["bstack:options"];
  const info = {
    fontName,
    browserName,
    browserVersion,
    os,
    osVersion,
  };
  let data = [];
  try {
    const fileContent = fs.readFileSync(SCREENSHOTS_REPORT_TMP_FILE);
    data = JSON.parse(fileContent);
  } catch {}
  data.push(info);
  fs.writeFileSync(SCREENSHOTS_REPORT_TMP_FILE, JSON.stringify(data));
};

const capabilitiesToScreenshotName = (browser, prefix = "") => {
  const { browserName, browserVersion } = browser.requestedCapabilities;
  const { os, osVersion } = browser.requestedCapabilities["bstack:options"];
  const name =
    `${prefix}-${os}-${osVersion}-${browserName}-${browserVersion}.png`.toLowerCase();
  return name.replace(/ +/, "-");
};

module.exports = { persistBrowserCapabilities, capabilitiesToScreenshotName };
