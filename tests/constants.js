const path = require("path");

const PROJECT_ID = 'gitlab-org/frontend/virtuoso';
const SCREENSHOTS_REPORT_TMP_FILE = path.join(
  __dirname,
  "gitlab_fonts_screenshots_report.json"
);

module.exports = {
  PROJECT_ID,
  SCREENSHOTS_REPORT_TMP_FILE,
};
