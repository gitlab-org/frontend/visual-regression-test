const {
  persistBrowserCapabilities,
  capabilitiesToScreenshotName,
} = require("./utils");

const FONT_NAME = "GitLab Sans";

describe(FONT_NAME, () => {
  // Maybe move this to some before hook?
  it("persists the browser's capabilities to a temporary file", async () => {
    persistBrowserCapabilities(FONT_NAME, browser);
  });

  it("should save a screenshot of the browser view", async () => {
    await browser.setWindowSize(1500, 3000);
    await browser.url(
      "https://gitlab-org.gitlab.io/frontend/fonts/specimen/gitlab-sans/"
    );
    const elem = await $(".setting.pb-3u.pt-3u");
    const screenshotName = capabilitiesToScreenshotName(browser, "gitlab-sans");
    await elem.saveScreenshot(`tests/screenshots/${screenshotName}`);
  });
});
