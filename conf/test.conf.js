const { config: baseConfig } = require("./base.conf.js");

let browsers;

try {
  browsers = require("./browsers.json");
} catch (e) {
  // fallback
  browsers = [
    {
      browserName: "chrome",
      browserVersion: "latest",
      "bstack:options": {
        os: "Windows",
        osVersion: "10",
      },
    },
  ];
}

const parallelConfig = {
  maxInstances: 5,
  maxInstancesPerCapability: 1,
  commonCapabilities: {
    "bstack:options": {
      buildName: "browserstack build",
      source: "webdriverio:sample-master:v1.2",
    },
  },
  services: [["browserstack", { buildIdentifier: "#${BUILD_NUMBER}" }]],
  capabilities: browsers,
};

exports.config = { ...baseConfig, ...parallelConfig };

// Code to support common capabilities
exports.config.capabilities.forEach(function (caps) {
  for (var i in exports.config.commonCapabilities)
    caps[i] = { ...caps[i], ...exports.config.commonCapabilities[i] };
});
