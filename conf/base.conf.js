const fs = require("fs");
const { SCREENSHOTS_REPORT_TMP_FILE } = require("../tests/constants");

exports.config = {
  user: process.env.BROWSERSTACK_USERNAME || "BROWSERSTACK_USERNAME",
  key: process.env.BROWSERSTACK_ACCESS_KEY || "BROWSERSTACK_ACCESS_KEY",

  updateJob: false,
  specs: ["../tests/*.spec.js"],
  exclude: [],

  logLevel: "warn",
  coloredLogs: true,
  screenshotPath: "./errorShots/",
  baseUrl: "",
  waitforTimeout: 10000,
  connectionRetryTimeout: 120000,
  connectionRetryCount: 3,
  hostname: "hub.browserstack.com",
  services: [["browserstack"]],
  framework: "mocha",
  mochaOpts: {
    ui: "bdd",
    timeout: 60000,
  },
  onPrepare: () => {
    try {
      fs.unlinkSync(SCREENSHOTS_REPORT_TMP_FILE);
    } catch {}
  },
};
