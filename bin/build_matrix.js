#!/usr/bin/env node
const http = require("axios");
const { uniqWith, isEqual } = require("lodash");

const TARGET_BROWSERS = ["chrome", "firefox", "safari"];
const MIN_WINDOWS_VERSION = 10;

async function getMacOSCodeNames() {
  const { data } = await http.get("https://endoflife.date/api/macos.json");
  return data.filter((x) => x.eol === false).map((x) => x.codename);
}

async function getBrowsers() {
  const { data } = await http.get(
    "https://api.browserstack.com/automate/browsers.json",
    {
      auth: {
        username: process.env.BROWSERSTACK_USERNAME || "BROWSERSTACK_USERNAME",
        password:
          process.env.BROWSERSTACK_ACCESS_KEY || "BROWSERSTACK_ACCESS_KEY",
      },
    },
  );
  return data;
}

async function main() {
  const [macOSWithSupport, browsers] = await Promise.all([
    getMacOSCodeNames(),
    getBrowsers(),
  ]);


  const filtered = browsers.filter((x) => {
    if (!TARGET_BROWSERS.includes(x.browser)) {
      return false;
    }

    if (x.os === "Windows") {
      return parseInt(x.os_version, 10) >= MIN_WINDOWS_VERSION;
    }

    if (x.os === "OS X") {
      return macOSWithSupport.includes(x.os_version);
    }

    return false;
  });

  return uniqWith(
    filtered.flatMap(({ os, os_version, browser }) => {
      return [
        {
          browserName: browser,
          browserVersion: "latest",
          "bstack:options": {
            os,
            osVersion: os_version,
          },
        },
      ];
    }),
    isEqual,
  );
}

main()
  .then((browsers) => {
    console.warn(`success, loaded ${browsers.length} browser configs`);
    console.log(JSON.stringify(browsers, null, 2));
  })
  .catch((e) => {
    console.error(e);
    process.exitCode = 1;
  });
