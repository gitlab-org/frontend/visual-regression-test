#!/usr/bin/env node

const fs = require("fs");
const { spawnSync } = require("child_process");
const axios = require("axios");
const {
  PROJECT_ID,
  SCREENSHOTS_REPORT_TMP_FILE,
} = require("../tests/constants");

const http = axios.create({
  baseURL: `https://gitlab.com/api/v4/projects/${encodeURIComponent(
    PROJECT_ID
  )}`,
  headers: {
    Authorization: `Bearer ${process.env.GITLAB_API_TOKEN}`,
  },
});

const getReport = () => {
  return JSON.parse(fs.readFileSync(SCREENSHOTS_REPORT_TMP_FILE));
};

const getFormattedReport = (report) => {
  let markdown = `| Font | OS | Browser |\n| - | - | - |\n`;

  report.forEach((testSuite) => {
    const { fontName, os, osVersion, browserName, browserVersion } = testSuite;
    markdown += `| ${fontName} | ${os} ${osVersion} | ${browserName} ${browserVersion} |\n`;
  });

  return markdown;
};

const createMR = async (description) => {
  const branchName = `update-screenshots-${Date.now()}`;
  spawnSync("git", ["checkout", "-b", branchName]);
  spawnSync("git", ["add", "tests/screenshots/"]);
  spawnSync("git", ["commit", "-m", "Update screenshots"]);
  spawnSync("git", [
    "push",
    "-u",
    `https://automated_merge_requests_creation_bot:${process.env.GITLAB_API_TOKEN}@gitlab.com/gitlab-org/frontend/visual-regression-test.git`,
    "HEAD",
  ]);
  await http.post(`/merge_requests`, {
    source_branch: branchName,
    target_branch: "main",
    title: "Update screenshots",
    description,
  });
  spawnSync("git", ["checkout", "-"]);
};

const main = () => {
  const report = getReport();
  const formattedReport = getFormattedReport(report);
  return createMR(formattedReport);
};

main().then(() => {
  console.log('Success')
}).catch(e => {
  console.warn('Error')
  console.error(e)
  process.exitCode = 1;
});
