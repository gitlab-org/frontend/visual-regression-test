# Virtuoso

This tool is responsible for performing visual regression tests of GitLab Fonts against the browsers
we support. Tests are run on a regular basis and a Merge Request is created
to update the screenshots, at which point we can assess whether or not the changes are acceptable.

## CI setup

For the screenshot update process to work properly in CI, a few CI variables need to be provided:
* `BROWSERSTACK_USERNAME`: The BrowserStack authentication username, can be found at https://www.browserstack.com/accounts/profile.
* `BROWSERSTACK_ACCESS_KEY`: The BrowserStack authentication access key, can be found at https://www.browserstack.com/accounts/profile.
* `GITLAB_API_TOKEN`: A project access tokens with the `api, read_repository` scope.

## How does this work?

This tool runs test suites against a set of operating systems and browsers combinations via
BrowserStack Automate. The test suites load our GitLab fonts specimen pages, capture screenshots of
portions of those pages and saves them locally. Screenshots are named in a way that ignores the
actual browser version so that we can compare the _current_ latest version against whatever was
considered the latest version the last time screenshots were updated.

## What does this _not_ do?

### Pixel-diffing

This tool currently does not support pixel-diffing. An early version of this leveraged Playwright
which supports pixel-diffing out of the box. We had to drop Playwright due to BrowserStack not
supporting bleeding-edge environments through it. We're now relying on Selenium which gives us access
to a greater choice of OSs and browsers. The drawback being that we have not yet implemented a way
to catch regressions automatically. Does therefore need to be checked manually in Merge Requests'
diff view.

### Linux testing

As of writing this, BrowserStack does not support Linux environments: https://www.browserstack.com/list-of-browsers-and-platforms/automate.
While Linux testing is definitely something we are interested in, we won't be supporting this just yet.
For the time being, we will be testing against macOS and Windows, and eventually Android and iOS.

## Updating the screenshots locally

1. Expose the necessary environment variables.
    ```sh
    export BROWSERSTACK_USERNAME="<username>"
    export BROWSERSTACK_ACCESS_KEY="<key>"
    ```
1. Install the dependencies.
    ```sh
    yarn
    ```
1. Run the test suite.
    ```sh
    yarn screenshots:update
    ```

